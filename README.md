# Quantizador

Projeto de criação de um quantizador em python
    
    - Reproduzir o Áudio e Plotar pedaços a padaços do todo (Sinal Original)
      - Franklin, Mikael
    - Quantizar o áudio a partir da taxa de quantização de bits
      - Mário
    - Mostrar o histograma do erro de quantização (sinal original - sinal quantizado)
      - João
    - Interface e selecionar o áudio
      - Kristhyan, Allef

Desenvolver uma aplicação em pyhton com interface gráfica, que deve:
- Mostrar trechos de um sinal de áudio lido a partir de um arquivo .wav 
- Aplicar nova quantização
- Mostrar o histograma do erro de quantização (sunal original - sinal quantizado)  
- Tocar o novo áudio quantizado  
- A aplicação deve mostrar novos trechos do sinal e gráficos periodicamente;  
- Deve ser possível modificar a resolução do quantizador em tempo real, e tocar o som com a nova quantização, e os novos gráficos.  
