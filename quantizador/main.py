import tkinter as tk
from tkinter import filedialog
from tkinter import ttk
from tkinter import filedialog
import matplotlib
import matplotlib.animation as animation
import wavio
import librosa
import numpy as np
import playsound as ps
import soundfile as sf
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

import config

# Definindo interface
janela_principal = tk.Tk()

DPI = janela_principal.winfo_fpixels("1i")

janela_principal.title("Quantizador de Áudio")
janela_principal.resizable(True, True)
janela_principal.config(bg="#cfcfcf")
janela_principal.resizable(0, 0)

largura_janela = janela_principal.winfo_screenwidth()
altura_janela = janela_principal.winfo_screenheight()

janela_principal.geometry("%dx%d+0+0" % (largura_janela, altura_janela))

largura_grafico = largura_janela * 0.75
altura_grafico = altura_janela * 0.85
figura = Figure(figsize=(largura_grafico / DPI, altura_grafico / DPI), dpi=DPI)
figura.patch.set_facecolor("#cfcfcf")
graficos = figura.subplots(3)

canvas = FigureCanvasTkAgg(figura, janela_principal)
canvas.get_tk_widget().place(x=1, y=1, relx=0.01, rely=0.01)

# Definindo as legendas do gráfico do sinal original
graficos[0].set_xlim(0, 1)
graficos[0].set_ylabel("Sinal Original", fontweight="bold")
graficos[0].set_title("Original", fontweight="bold", fontsize=10)
graficos[0].grid(True)

graficos[1].set_xlim(0, 1)
graficos[1].set_ylabel("Sinal Quantizado", fontweight="bold")
graficos[1].set_title("Quantizado", fontweight="bold", fontsize=10)
graficos[1].grid(True)

graficos[2].set_xlim(0, 1)
graficos[2].set_ylabel("Taxa de Erro", fontweight="bold")
graficos[2].set_xlabel("Tempo", fontweight="bold")
graficos[2].set_title("Erro", fontweight="bold", fontsize=10)
graficos[2].grid(True)


# Função de buscar o arquivo
def procurar_arquivo():
    config.DIRETORIO_ARQUIVO_AUDIO_WAV = filedialog.askopenfilename(
        title="Selecione o arquivo",
        filetypes=(("Wav", "*.wav"), ("Todos os formatos", "*.*")),
    )

    if len(config.DIRETORIO_ARQUIVO_AUDIO_WAV) > 0:
        # Informa o nome do aruiqvo selecionado
        arquivo_audio_wav_frame.configure(
            text=config.DIRETORIO_ARQUIVO_AUDIO_WAV.split("/")[-1]
        )
        ler_audio_original()
        sinal_quantizado_sinal_uniform(
            sinal_original=config.SINAL,
            nivel_minimo=-1,
            nivel_maximo=1,
            numero_de_niveis=2 ** config.TAXA_DE_QUANTIZACAO,
        )
        config.NOVO_SINAL = True

        botao_tocar_original["state"] = "normal"
        botao_tocar_quantizado["state"] = "normal"
        taxa_quantizacao["state"] = "normal"


# lê o sinal de audio original para extair as amplitudes do sinal,
# taxa de amostragem, tempo de duração do sinal e definir o período de amostragem
def ler_audio_original():
    config.SINAL, config.TAXA_DE_AMOSTRAGEM = sf.read(
        config.DIRETORIO_ARQUIVO_AUDIO_WAV
    )
    config.PERIODO_DE_AMOSTRAGEM = 1 / config.TAXA_DE_AMOSTRAGEM
    config.TEMPO = np.arange(
        0,
        len(config.SINAL) * config.PERIODO_DE_AMOSTRAGEM,
        config.PERIODO_DE_AMOSTRAGEM,
    )


# limpa o grafico do audio original para que seja atualizado
def limpar_grafico_original():
    graficos[0].clear()
    graficos[0].set_xlim(0, 1)
    graficos[0].set_ylabel("Sinal Original", fontweight="bold")
    graficos[0].set_title("Original", fontweight="bold", fontsize=10)
    graficos[0].grid(True)


def limpar_grafico_quantizado():
    graficos[1].clear()
    graficos[1].set_xlim(0, 1)
    graficos[1].set_ylabel("Sinal Quantizado", fontweight="bold")
    graficos[1].set_title("Quantizado", fontweight="bold", fontsize=10)
    graficos[1].grid(True)


def limpar_grafico_erro():
    graficos[2].clear()
    graficos[2].set_xlim(0, 1)
    graficos[2].set_ylabel("Taxa de Erro", fontweight="bold")
    graficos[2].set_title("Original", fontweight="bold", fontsize=10)
    graficos[2].grid(True)


# plota o grafico do sinal de audio original
def gerar_grafico_sinal_original():
    if config.TEMPO is not None and config.SINAL is not None:
        limpar_grafico_original()
        graficos[0].plot(config.TEMPO, config.SINAL)


# quantiza o sinal e salva o audio
def sinal_quantizado_sinal_uniform(
    sinal_original, nivel_minimo=-1, nivel_maximo=1, numero_de_niveis=256
):

    sinal_normalizado = (
        (sinal_original - nivel_minimo)
        * (numero_de_niveis - 1)
        / (nivel_maximo - nivel_minimo)
    )
    sinal_normalizado[sinal_normalizado > numero_de_niveis - 1] = numero_de_niveis - 1
    sinal_normalizado[sinal_normalizado < 0] = 0
    x_normalize_quant = np.around(sinal_normalizado)
    x_quant = (x_normalize_quant) * (nivel_maximo - nivel_minimo) / (
        numero_de_niveis - 1
    ) + nivel_minimo

    config.DIRETORIO_ARQUIVO_AUDIO_QUANT_WAV = "audio_quantizado.wav"
    x, Fs = librosa.load(config.DIRETORIO_ARQUIVO_AUDIO_WAV)
    wavio.write(config.DIRETORIO_ARQUIVO_AUDIO_QUANT_WAV, x_quant, Fs, sampwidth=2)

    config.SINAL_QUANTIZADO = x_quant


def gerar_grafico_sinal_quantizado():
    if config.TEMPO is not None and config.SINAL is not None:
        limpar_grafico_quantizado()
        graficos[1].plot(
            config.TEMPO,
            config.SINAL_QUANTIZADO,
        )


def plota_erro(nivel_minimo=-1, nivel_maximo=1, numero_de_niveis=256):
    if config.TEMPO is not None and config.SINAL is not None:

        limpar_grafico_erro()
        x = np.linspace(nivel_minimo, nivel_maximo, 1000)

        ## config.SINAL_QUANTIZADO = sinal_quantizado_sinal_uniform(
        ##     sinal_original=config.SINAL,
        ##     nivel_minimo=nivel_minimo,
        ##     nivel_maximo=nivel_maximo,
        ##     numero_de_niveis=numero_de_niveis,
        ## )
        quant_stepsize = (nivel_maximo - nivel_minimo) / (numero_de_niveis - 1)
        title = r"$\lambda = %d, \Delta=%0.2f$" % (numero_de_niveis, quant_stepsize)

        error = np.abs(config.SINAL_QUANTIZADO - config.SINAL)
        ## graficos[2].plot(config.TEMPO, config.SINAL, color="k", label="Sinal Original")
        ## graficos[2].plot(
        ##     config.TEMPO,
        ##     config.SINAL_QUANTIZADO,
        ##     color="b",
        ##     label="Sinal Quantizado",
        ## )
        graficos[2].plot(config.TEMPO, error, "r--", label="Taxa de Erro")
        graficos[2].set_title(title)
        graficos[2].set_xlabel("Amplitude")
        graficos[2].set_ylabel("Taxa de Erro")
        graficos[2].grid("on")


# Função do botão tocar original
def tocar_original():
    ps.playsound(config.DIRETORIO_ARQUIVO_AUDIO_WAV)


# Função do botão tocar quantizado
def tocar_quantizado():
    ps.playsound(config.DIRETORIO_ARQUIVO_AUDIO_QUANT_WAV)


# Função inserir a taxa de quantização
def set_taxa_quantizacao(event):
    config.TAXA_DE_QUANTIZACAO = int(taxa_quantizacao.get())
    print(config.TAXA_DE_QUANTIZACAO)
    taxa_quantizacao_label["text"] = "Taxa de quantização\n" + str(
        config.TAXA_DE_QUANTIZACAO
    )
    taxa_quantizacao.delete(0, tk.END)
    sinal_quantizado_sinal_uniform(
        sinal_original=config.SINAL,
        nivel_minimo=-1,
        nivel_maximo=1,
        numero_de_niveis=2 ** config.TAXA_DE_QUANTIZACAO,
    )
    config.NOVO_SINAL = True


arquivo_audio_wav_botao = tk.Button(
    janela_principal,
    width=21,
    height=1,
    font=13,
    fg="#000000",
    relief="groove",
    text="Selecionar arquivo",
    command=procurar_arquivo,
)
arquivo_audio_wav_botao.place(
    in_=janela_principal, relx=0.82, rely=0.23, anchor=tk.CENTER
)

tk.Label(
    janela_principal,
    text="Arquivo selecionado",
    bg="#cfcfcf",
    fg="#000",
    font=13,
    width=21,
).place(relx=0.82, rely=0.28, anchor=tk.CENTER)

arquivo_audio_wav_frame = tk.Label(
    janela_principal,
    font=13,
    borderwidth=0,
    bg="#cfcfcf",
    bd=1,
)
arquivo_audio_wav_frame.place(
    in_=janela_principal, relx=0.82, rely=0.31, anchor=tk.CENTER
)

# as funçoes que plotam os graficos do audio original,
# audio quantizado e histograma de erro devem ser
# chamadas dentro dessa função
def renderizar_graficos(i):
    if config.NOVO_SINAL:
        print("gerando")
        gerar_grafico_sinal_original()
        gerar_grafico_sinal_quantizado()
        plota_erro()
        config.NOVO_SINAL = False


# atualiza os graficos passados na função renderizar_gráficos
ani1 = animation.FuncAnimation(figura, renderizar_graficos, interval=1000)


# Label da taxa de quantização
taxa_quantizacao_label = tk.Label(
    janela_principal,
    text="Taxa de quantização\n" + str(config.TAXA_DE_QUANTIZACAO),
    bg="#cfcfcf",
    fg="#000",
    font=13,
    width=21,
    height=2,
)
taxa_quantizacao_label.place(relx=0.82, rely=0.39, anchor=tk.CENTER)


# Botões para reprodução do áudio
botao_tocar_original = tk.Button(
    janela_principal,
    relief="groove",
    text="Tocar áudio original",
    font=13,
    command=tocar_original,
)
botao_tocar_original.place(relx=0.82, rely=0.52, anchor=tk.CENTER)

botao_tocar_quantizado = tk.Button(
    janela_principal,
    relief="groove",
    text="Tocar áudio equalizado",
    font=13,
    command=tocar_quantizado,
)
botao_tocar_quantizado.place(relx=0.82, rely=0.57, anchor=tk.CENTER)


taxa_quantizacao = tk.Entry(janela_principal, state="disabled")
taxa_quantizacao.bind("<Return>", set_taxa_quantizacao)
taxa_quantizacao.place(relx=0.82, rely=0.43, anchor=tk.CENTER)

# Desabilita botões
botao_tocar_original["state"] = "disabled"
botao_tocar_quantizado["state"] = "disabled"

# Footer
tk.Label(
    janela_principal,
    text="Desenvolvido pelos discentes de Eng. da Computação - 2017",
    bg="#cfcfcf",
    fg="#000",
    font=("Helvetica", 18, "bold"),
).place(relx=0.37, rely=0.84, anchor=tk.CENTER)
tk.Label(
    janela_principal,
    text="Allef Jardim Vieira Fonseca - Bryan Franklin Sena de Sousa - João Vitor Farias de Castro",
    bg="#cfcfcf",
    fg="#000",
    font=("Helvetica", 11),
).place(relx=0.37, rely=0.88, anchor=tk.CENTER)
tk.Label(
    janela_principal,
    text="Kristhyan de Matos Maia - Mário Victor Ribeiro Silva - Mikael Almondes da Silva",
    bg="#cfcfcf",
    fg="#000",
    font=("Helvetica", 11),
).place(relx=0.37, rely=0.91, anchor=tk.CENTER)

# Imagens
img1 = tk.PhotoImage(file="../quantizador/quantizador/imagem/unifesspa.png")
imagem_unifesspa = tk.Label(janela_principal, image=img1, bg="#cfcfcf")
imagem_unifesspa.place(relx=0.75, rely=0.86, anchor=tk.CENTER)

img2 = tk.PhotoImage(file="../quantizador/quantizador/imagem/faceel.png")
imagem_unifesspa = tk.Label(janela_principal, image=img2, bg="#cfcfcf")
imagem_unifesspa.place(relx=0.87, rely=0.86, anchor=tk.CENTER)


janela_principal.mainloop()
